﻿namespace Study.App.Lessons
{
    using System.IO;
    using System.Net;

    public class Weather1
    {
        public string GetNowVolgograd()
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("X-Gismeteo-Token", "56b30cb255.3443075");

                using (var data = client.OpenRead("https://api.gismeteo.net/v2/weather/current/5089/"))
                using (var reader = new StreamReader(data))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}
