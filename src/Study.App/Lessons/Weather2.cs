﻿namespace Study.App.Lessons
{
    using System.IO;
    using System.Net;

    public class Weather2
    {
        private readonly int volgogradId = 5089;
        private readonly int moskowId = 5678;

        private readonly GismeteoAdapter2 gismeteoAdapter = new GismeteoAdapter2();

        public string GetNowVolgograd()
        {
            return gismeteoAdapter.GetCurrentWeather(volgogradId);
        }

        public string GetNowMoscow()
        {
            return gismeteoAdapter.GetCurrentWeather(moskowId);
        }
    }

    /// <summary>
    /// https://www.gismeteo.ru/api/
    /// </summary>
    public class GismeteoAdapter2
    {
        public string GetCurrentWeather(int id)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("X-Gismeteo-Token", "56b30cb255.3443075");

                using (var data = client.OpenRead($"https://api.gismeteo.net/v2/weather/current/{id}/"))
                using (var reader = new StreamReader(data))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}
