﻿namespace Study.App.Lessons
{
    using System.Text.Json;
    using System.IO;
    using System.Net;

    public class Weather3
    {
        private readonly IWeatherAdapter _adapter;

        public Weather3(IWeatherAdapter adapter)
        {
            _adapter = adapter;
        }

        public string GetNowVolgograd()
        {
            var res = _adapter.GetCurrentWeather("Volgograd");

            return $"VLG:{res}";
        }

        public string GetNowMoscow()
        {
            var res = _adapter.GetCurrentWeather("Moskow");

            return $"MSK:{res}";
        }

        public string GetNow(string city)
        {
            return _adapter.GetCurrentWeather("Moskow");
        }
    }

    public interface IWeatherAdapter
    {
        public string GetCurrentWeather(string city);
    }

    /// <summary>
    /// https://www.gismeteo.ru/api/
    /// </summary>
    internal class GismeteoAdapter3 : IWeatherAdapter
    {
        public string GetCurrentWeather(string city)
        {
            var cityId = SearchCity(city);
            return GetCurrentWeather(cityId);
        }

        public int SearchCity(string text)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("X-Gismeteo-Token", "56b30cb255.3443075");

                using (var data = client.OpenRead($"https://api.gismeteo.net/v2/search/cities/?lang=en&query={text}"))
                using (var reader = new StreamReader(data))
                {
                    var res = reader.ReadToEnd();
                    var res2 = JsonSerializer.Deserialize<GismeteoSearchResult>(res);

                    return res2.id;
                }
            }
        }

        public string GetCurrentWeather(int id)
        {
            using (var client = new WebClient())
            {
                client.Headers.Add("Accept-Encoding", "gzip");
                client.Headers.Add("X-Gismeteo-Token", "56b30cb255.3443075");

                using (var data = client.OpenRead($"https://api.gismeteo.net/v2/weather/current/{id}/"))
                using (var reader = new StreamReader(data))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }

    internal class GismeteoSearchResult
    {
        public int id { get; set; }

        public string name { get; set; }

        public string url { get; set; }

        public string kind { get; set; }
    }

    internal class Rp5Adapter3 : IWeatherAdapter
    {
        public string GetCurrentWeather(string city)
        {
            throw new System.NotImplementedException();
        }
    }
}