namespace Study.App
{
    using bgTeam;
    using Microsoft.Extensions.Logging;
    using System;
    using Study.App.Lessons;
    using System.Net;
    using System.IO;

    internal class Runner
    {
        private readonly ILogger<Runner> _logger;
        private readonly IStoryBuilder _storyBuilder;

        public Runner(
            ILogger<Runner> logger,
            IStoryBuilder storyBuilder)
        {
            _logger = logger;
            _storyBuilder = storyBuilder;
        }

        public void Run()
        {
            var weather = new Weather1();

            var res = weather.GetNowVolgograd();

            Console.WriteLine(res);
        }

        //public void Run()
        //{
        //    using (var client = new WebClient())
        //    {
        //        client.Headers.Add("Accept-Encoding", "gzip");
        //        client.Headers.Add("X-Gismeteo-Token", "56b30cb255.3443075");

        //        using (var data = client.OpenRead("https://api.gismeteo.net/v2/weather/current/5089/"))
        //        using (var reader = new StreamReader(data))
        //        {
        //            Console.WriteLine(reader.ReadToEnd());
        //        }
        //    }
        //}

        /// <summary>
        /// lesson 2
        /// </summary>
        /// <returns></returns>
        //public async Task Run()
        //{
        //    _logger.LogInformation("start");

        //    var context = new TestStoryContext();

        //    await _storyBuilder
        //        .Build(context)
        //        .ReturnAsync<bool>();
        //}
    }
}