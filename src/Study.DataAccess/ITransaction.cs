namespace Study.DataAccess
{
    using System;

    public interface ITransaction : IDisposable
    {
        Guid TransactionId { get; }

        void Commit();

        void Rollback();
    }
}