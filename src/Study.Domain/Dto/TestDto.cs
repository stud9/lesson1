namespace Study.Domain.Dto
{
    public class TestDto
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}