namespace Study.Domain
{
    public interface IEntity
    {
        long? Id { get; set; }
    }
}