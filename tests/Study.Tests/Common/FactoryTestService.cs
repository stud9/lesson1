namespace Study.Tests.Common
{
    using bgTeam.DataAccess;
    using bgTeam.DataAccess.Impl.Dapper;
    using bgTeam.DataAccess.Impl.MsSql;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;

    internal class FactoryTestService
    {
        public ILoggerFactory LoggerFactory { get; private set; }

        public IRepository Repository { get; private set; }

        public ICrudService CrudService { get; private set; }

        public FactoryTestService()
        {
            LoggerFactory = Microsoft.Extensions.Logging.LoggerFactory.Create(_ => { });

            var env = "Development";
            var appConfiguration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env}.json")
                .AddJsonFile($"connectionStrings.{env}.json", optional: true)
                .AddJsonFile($"serilog.{env}.json", optional: true)
                .Build();

            var connectionFactory = new ConnectionFactoryMsSql(appConfiguration.GetConnectionString("MAINDB"));

            var logger = LoggerFactory.CreateLogger<RepositoryDapper>();

            Repository = new RepositoryDapper(connectionFactory);

            CrudService = new CrudServiceDapper(connectionFactory);
        }
    }
}
