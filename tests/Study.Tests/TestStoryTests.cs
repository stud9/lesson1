namespace Study.Tests
{
    using Study.Story;
    using Study.Tests.Common;
    using Microsoft.Extensions.Logging;
    using Xunit;
    using Study.App.Lessons;
    using System.Net;

    public class TestStoryTests
    {
        private readonly FactoryTestService _factory;

        public TestStoryTests()
        {
            _factory = new FactoryTestService();
        }

        [Fact]
        public void Test1()
        {
            var story = new TestStory(_factory.LoggerFactory.CreateLogger<TestStory>(), _factory.Repository, _factory.CrudService);
            Assert.NotNull(story);
        }

        [Fact]
        public void Test2()
        {
            var weather = new Weather1();

            Assert.Throws<WebException>(() => weather.GetNowVolgograd());
        }

        [Fact]
        public void Test3()
        {
            var adapter = new TestAdapter();
            var weather = new Weather3(adapter);

            Assert.Equal("MSK:-1", weather.GetNowMoscow());
            Assert.Equal("VLG:-1", weather.GetNowVolgograd());
        }
    }


    internal class TestAdapter : IWeatherAdapter
    {
        public string GetCurrentWeather(string city)
        {
            return "-1";
        }
    }
}
